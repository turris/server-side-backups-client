from setuptools import setup
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.rst')) as f:
    long_description = f.read()

# Get the license and append to long description
with open(path.join(here, 'LICENSE.txt')) as f:
    long_description += f.read()

setup(
    name='ssbackups-client',
    version='2.0',
    description="CLI client to manage Turris router's backups via REST API",
    long_description=long_description,
    author='CZ.NIC, z. s. p. o.',
    author_email='tech.support@turris.cz',
    url="https://gitlab.labs.nic.cz/turris/server-side-backups-client/",
    license='GPL-3.0',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: System Administrators',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)'
        'Programming Language :: Python',
        'Topic :: System :: Archiving :: Backup',
        'Topic :: Utilities'

    ],
    keywords='server side backups',
    packages=['ssbackups_client'],
    entry_points={
        "console_scripts": [
            "ssbackups = ssbackups_client.__main__:main",
        ]
    },
)
