"""Tests for ssbackups module

There are tests
    1) on structure of ClientException
    2) on validate type function type_target_path
    3) on validate type function type_filepath
    4) on removing files when remove_encrypted_backup was called
    5) on raising SSBackupsException exception when encrypt_backups was called
    6) on raising SSBackupsException exception when decrypt_backups was called
    7) on raising SSBackupsException exception when get_registration_code was called
    8) on raising SSBackupsException exception when call_rest_api was called
"""

import unittest
import json
from subprocess import CalledProcessError
from mock import patch

from ssbackups_client import SSBackupsException, \
    remove_encrypted_backup, encrypt_backup, decrypt_backup, get_registration_code, call_rest_api, \
    backup_create, ERR_CODE_MAX_FILE_SIZE, ERR_CODE_API_CALL
from ssbackups_client.__main__ import type_target_path, type_filepath


# test preparation things
JSON_500_SERVER_ERROR = '{"status":500, "detail": "Internal Server Error", "exception": "unknown"}'
JSON_201_BACKUP_CREATED = '{"id": 1}'


class TestClientException(unittest.TestCase):
    """Some tests to test base exception for the project - ClientException
    """

    def test_exception_structure(self):
        """
        What if something wrong happend and from server arrives JSON_500_SERVER_ERROR?
        :return:
        """
        # pylint: disable=missing-docstring
        def raise_exception(response):
            raise SSBackupsException(response)

        self.assertRaises(
            SSBackupsException,
            raise_exception,
            json.loads(JSON_500_SERVER_ERROR)
        )
        try:
            raise_exception(json.loads(JSON_500_SERVER_ERROR))
        except SSBackupsException as exc:
            self.assertTrue(hasattr(exc, 'detail'))
            self.assertTrue(hasattr(exc, 'code'))


class TestTypeTargetPath(unittest.TestCase):
    """
    Some tests to test if validator on directory target path arrived from CLI works correctly - directory exists
    """

    def test_raises_exception_on_path_ends_with_slash(self):
        """
        What happend if a file path of backup end with slash? SSBackupsException exception is raised...
        :return:
        """
        self.assertRaises(
            SSBackupsException,
            type_target_path,
            '/tmp/backup/'
        )

    def test_raises_exception_on_non_existent_directory(self):
        """
        What happend if a file path of backup points to non-existent directory?
        SSBackupsException exception is raised..
        :return:
        """
        self.assertRaises(
            SSBackupsException,
            type_target_path,
            '/tmp/non-existent-directory/backup'
        )


class TestTypeFilePath(unittest.TestCase):
    """
    Some tests to test if validator on file path arrived from CLI works correctly - file exists
    """

    def test_raises_exception_on_non_existent_file(self):
        """
        What happend if backup is non-existant file? SSBackupsException exception is raised...
        :return:
        """
        self.assertRaises(
            SSBackupsException,
            type_filepath,
            '/home/non-existent-directory/backup'
        )


class TestRemoveEncryptedBackup(unittest.TestCase):
    """Some tests to assure that os.remove was called to remove backup from disk
    """

    @patch('ssbackups_client.os.remove')
    def test_remove_backup(self, mock_remove):
        """
        Did os.remove be called when remove_encrypted_backup called with backup keyword argument?

        :param mock_remove: it is mocked os.remove call.
        :return:
        """
        backup = '/tmp/backup.gpg'
        with open(backup, 'wb') as backup_file_handler:
            backup_file_handler.write(b'Hello world')

        remove_encrypted_backup(backup)

        self.assertTrue(mock_remove.called)

    @patch('ssbackups_client.os.remove')
    def test_remove_target(self, mock_remove):
        """
        Did os.remove be called when remove_encrypted_backup called with target keyword argument?

        :param mock_remove: it is mocked os.remove call.
        :return:
        """
        target = '/tmp/backup.gpg'
        with open(target, 'wb') as target_file_handler:
            target_file_handler.write(b'Hello world')

        remove_encrypted_backup(target)

        self.assertTrue(mock_remove.called)


class TestEncryptBackup(unittest.TestCase):
    """
    Some tests to test encrypt backup raises SSBackupsException exception when Popen.communicate returns '*failed*'.
    """

    @patch('ssbackups_client.Popen.communicate', return_value=('', 'something has failed'))
    def test_raises_client_exception(self, mock_process_communicate):
        """
        While encrypt_backup calling some bad things happend...Will SSBackupsException exception be raised?
        :param mock_process_communicate: it is mocked Popen.communicate call
        :return:
        """
        self.assertRaises(
            SSBackupsException,
            encrypt_backup,
            '/tmp/backup',
            'secret'
        )
        self.assertTrue(mock_process_communicate.called)


class TestDecryptBackup(unittest.TestCase):
    """
    Some tests to test decrypt backup raises SSBackupsException exception when Popen.communicate returns '*failed*'.
    """

    @patch('ssbackups_client.Popen.communicate', return_value=('', 'something has failed'))
    def test_raises_client_exception(self, mock_process_communicate):
        """
        While encrypt_backup calling some bad things happend... Will SSBackupsException exception be raised?
        :param mock_process_communicate: it is mocked Popen.communicate call
        :return:
        """
        self.assertRaises(
            SSBackupsException,
            decrypt_backup,
            '/tmp/backup',
            b'<some binary data>',
            'secret'
        )
        self.assertTrue(mock_process_communicate.called)


class TestGetRegistrationCode(unittest.TestCase):
    """
    Some tests to test retrieving router registration code on raising SSBackupsException
    when Popen.communicate returns '*failed*'.
    """

    @patch('ssbackups_client.open', side_effect=IOError(2, 'No such file or directory', '/tmp/backup'))
    def test_raise_exception_on_attemp_to_get_registration_code(self, mock_open):
        """
        You needs registration code and when something went wrong then
        SSBackupsException exception should be raised...
        :param mock_open: it is mocked Popen.communicate call
        :return:
        """
        self.assertRaises(
            SSBackupsException,
            get_registration_code,
            '/tmp/registration_code'
        )
        self.assertTrue(mock_open.called)


class TestCallRestApi(unittest.TestCase):
    """
    Some tests to test call_rest_api raising ClientException
    """

    TEST_VALUE_1 = ('{"payload":["backup reaches or crosses over the limit max file size"],'
                    '"detail":"client error","status":400,"exception":"ValidationError"}'
                    '\n400\napplication/json')
    TEST_VALUE_2 = ('<html>\n<head><title>413 Request Entity Too Large</title></head>\n'
                    '<body bgcolor="white">\n'
                    '<center><h1>413 Request Entity Too Large</h1></center>\n'
                    '<hr><center>nginx/1.10.3</center>\n'
                    '</body>\n'
                    '</html>\n'
                    '\n'
                    '413\n'
                    'text/html')
    TEST_VALUE_3 = 'Some unexpected text\n\n500\ntext/html'
    TEST_EXCEPTION_1 = SSBackupsException("Max file size reached.", ERR_CODE_MAX_FILE_SIZE)
    TEST_EXCEPTION_2 = SSBackupsException("Connection error", ERR_CODE_API_CALL)

    @patch(
        'ssbackups_client.check_output',
        side_effect=CalledProcessError(-1, 'echo "hello world!"', 'something has failed')
    )
    def test_raise_exception_on_bad_call(self, mock_check_output):
        """
        What happend if user try to call remote REST API? And when something wrong happened then
        SSBackupsException exception should be raised...
        :param mock_check_output: it is mocked Popen.communicate call
        :return:
        """
        self.assertRaises(
            SSBackupsException,
            call_rest_api,
            '50426AEB539CF8EA',
            'https://rb.turris.cz',
            'list'
        )
        self.assertTrue(mock_check_output.called)

    def test_raise_exception_on_bad_http_status(self):
        """
        What happend if user try to call remote REST API and 4xx or 5xx HTTP status code comes back?
        SSBackupsException exception should be raised...
        Normally curl gives no sign when 4xx or 5xx HTTP status code comes back, but when --fail options
        is used, curl returns non-zero exit code and subprocess library running curl raises CalledProcessException
        which is caught by 'except CalledProcessError' in call_rest_api function ...
        :return:
        """
        self.assertRaises(
            SSBackupsException,
            call_rest_api,
            'bad-registration-code',
            'https://rb.turris.cz',
            'list'
        )

    @patch('ssbackups_client.call_rest_api', return_value=TEST_VALUE_1)
    @patch('ssbackups_client.encrypt_backup', return_value=None)
    @patch('ssbackups_client.remove_encrypted_backup', return_value=None)
    def test_raise_max_file_size_error(self, mock_remove_encrypted_backup, mock_encrypt_backup,
                                       mock_call_rest_api):
        """
        What happend if user try to store backup larger than max file size set by Django?
        SSBackupsException exception should be raised...
        :return:
        """
        try:
            backup_create(
                "registration-code", "https://rb.turris.cz", '/tmp/backup.tar.gz', 'password')
        except SSBackupsException as exc:
            self.assertEqual(exc.detail, self.TEST_EXCEPTION_1.detail)
            self.assertEqual(exc.code, self.TEST_EXCEPTION_1.code)

        self.assertTrue(mock_remove_encrypted_backup.called)
        self.assertTrue(mock_encrypt_backup.called)
        self.assertTrue(mock_call_rest_api.called)

    @patch('ssbackups_client.call_rest_api', return_value=TEST_VALUE_2)
    @patch('ssbackups_client.encrypt_backup', return_value=None)
    @patch('ssbackups_client.remove_encrypted_backup', return_value=None)
    def test_raise_max_file_size_error_text_html(self, mock_remove_encrypted_backup,
                                                 mock_encrypt_backup, mock_call_rest_api):
        """
        What happend if user try to store backup larger than max file size set by Nginx?
        SSBackupsException exception should be raised...
        :return:
        """
        try:
            backup_create(
                "registration-code", "https://rb.turris.cz", '/tmp/backup.tar.gz', 'password')
        except SSBackupsException as exc:
            self.assertEqual(exc.detail, self.TEST_EXCEPTION_1.detail)
            self.assertEqual(exc.code, self.TEST_EXCEPTION_1.code)

        self.assertTrue(mock_remove_encrypted_backup.called)
        self.assertTrue(mock_encrypt_backup.called)
        self.assertTrue(mock_call_rest_api.called)

    @patch('ssbackups_client.call_rest_api', return_value=TEST_VALUE_3)
    @patch('ssbackups_client.encrypt_backup', return_value=None)
    @patch('ssbackups_client.remove_encrypted_backup', return_value=None)
    def test_raise_error_text_html(self, mock_remove_encrypted_backup, mock_encrypt_backup,
                                   mock_call_rest_api):
        """
        What happend if user try to store backup and unexptected HTTP text/html response arrives?
        SSBackupsException exception should be raised...
        :return:
        """
        try:
            backup_create(
                "registration-code", "https://rb.turris.cz", '/tmp/backup.tar.gz', 'password')
        except SSBackupsException as exc:
            self.assertEqual(exc.detail, self.TEST_EXCEPTION_2.detail)
            self.assertEqual(exc.code, self.TEST_EXCEPTION_2.code)

        self.assertTrue(mock_remove_encrypted_backup.called)
        self.assertTrue(mock_encrypt_backup.called)
        self.assertTrue(mock_call_rest_api.called)
