#
# The client server-side-backups app is tool to manage your router's backups and thus it is a proxy
# to call remote API.
#
# Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import json
import os
import re

from sys import stdout

from argparse import ArgumentParser

from . import (
    SSBackupsException, ERR_CODE_ARGS, ERR_CODE_UNKNOWN, get_registration_code,
    backup_create, backup_retrieve, backup_delete, backup_ondemand, backup_list,
)


def type_target_path(value):
    """
    Make sure that value is file path and directory of that file exists or SSBackupsException
    is raised.
    :param value: a directory path
    :return: a directory path
    """
    if not value.split(os.path.sep)[-1]:
        raise SSBackupsException(
            "Given file path needn't slash at the end of the path!", ERR_CODE_ARGS)
    if not os.path.isdir(os.path.dirname(value)):
        raise SSBackupsException(
            "Given file path has no existing directory!", ERR_CODE_ARGS)
    return value


def type_filepath(value):
    """
    Make sure that value is filepath or SSBackupsException is raised.
    :param value: a file path
    :return: a file path
    """
    if os.path.isfile(value):
        return value
    raise SSBackupsException("Given path is not the existing path to any file!", ERR_CODE_ARGS)


def type_url(value):
    """
    Make sure that value is URL in form or meaning of scheme://hostname:port
    :param value:  Remote URL
    :return: Remote URL
    """
    error = False
    url_parts = value.split(':')
    if url_parts[0] not in ('http', 'https'):
        # URL scheme is not valid
        error = True
    elif 1 < len(url_parts) < 4:
        if not url_parts[1].startswith('//'):
            # before hostname must be double slash
            error = True
        if len(url_parts) == 3:
            if not re.match(r"[0-9]{2,5}", url_parts[2]):
                # port is not a number
                error = True
    else:
        # count of parts of URL is not valid
        error = True
    if error:
        raise SSBackupsException("Bad value of URL!", ERR_CODE_ARGS)
    return value


def main():
    # exit code when it is sunny days
    exit_code = 0

    try:
        parser = ArgumentParser(description=(
            "It manages backups of Turris router. It allows to list, create, delete, "
            "retrieve and mark stable router's backups. Behind the every each action "
            "is remote API call. Password to encrypt or decrypt backup can be passed "
            "via -w option or via prompt while command runs.")
        )
        parser.add_argument(
            '-t', '--connection-timeout',
            dest='connection_timeout',
            default=10,
            type=int,
            metavar='SECONDS',
            help="Timeout for http requests"
        )
        parser.add_argument(
            '-u', '--url',
            dest='url',
            default='https://rb.turris.cz',
            type=type_url,
            metavar='URL',
            help="URL of remote server-side-backups API - scheme://hostname:port"
        )
        parser.add_argument(
            '-r', '--reg-code-path',
            dest='reg_code_path',
            default='/usr/share/server-uplink/registration_code',
            type=type_filepath,
            metavar='FILE',
            help="File path to registration code"
        )
        parser.add_argument(
            '-w', '--passwd',
            dest='passwd',
            default=None,
            type=str,
            metavar='PASSWORD',
            help="Password to encrypt a backup"
        )

        # subparsers to dinstinguish between actions
        subparsers = parser.add_subparsers(
            description="Managing router backups on the server side",
            help="Possible actions to do"
        )
        # action --list does not need additional arguments
        parser_list = subparsers.add_parser(
            'list', help="list all of the backups on the server-side")
        parser_list.set_defaults(action='list')

        # action --create requires backup argument
        parser_create = subparsers.add_parser(
            'create', help="encrypt and upload backup to server side")
        parser_create.set_defaults(action='create')
        parser_create.add_argument(
            'backup',
            type=type_filepath,
            metavar='FILE',
            help="Path to a backup file"
        )

        # action --delete requires backup_id argument
        parser_delete = subparsers.add_parser('delete', help="delete backup forever on server side")
        parser_delete.set_defaults(action='delete')
        parser_delete.add_argument(
            'backup_id',
            type=int,
            metavar='ID',
            help="Id of a backup known to server-side-backups server",
        )

        # action --retrieve requires backup_id and target arguments
        parser_retrieve = subparsers.add_parser(
            'retrieve', help="download backup and decrypt from server side")
        parser_retrieve.set_defaults(action='retrieve')
        parser_retrieve.add_argument(
            'backup_id',
            type=int,
            metavar='ID',
            help="Id of a backup known to server-side-backups server",
        )
        parser_retrieve.add_argument(
            'target',
            type=type_target_path,
            metavar='FILE',
            help="Path to a file where backup will be saved"
        )

        # action --ondemand requires backup_id
        parser_ondemand = subparsers.add_parser(
            'ondemand', help="mark backup as stable on server side")
        parser_ondemand.set_defaults(action='ondemand')
        parser_ondemand.add_argument(
            'backup_id',
            type=int,
            metavar='ID',
            help="Id of a backup known to server-side-backups server"
        )

        # get arguments
        cli_args = parser.parse_args()

        connection_timeout = cli_args.connection_timeout

        # get registration code first because everytime is needed
        registration_code = get_registration_code(cli_args.reg_code_path)

        response = None

        if cli_args.action == 'create':
            # returns JSON {"id": x}
            response = backup_create(
                registration_code,
                cli_args.url,
                cli_args.backup,
                cli_args.passwd,
                timeout=connection_timeout,
            )

        elif cli_args.action == 'retrieve':
            backup_retrieve(
                registration_code,
                cli_args.url,
                cli_args.backup_id,
                cli_args.target,
                cli_args.passwd,
                timeout=connection_timeout,
            )

        elif cli_args.action == 'delete':
            backup_delete(
                registration_code,
                cli_args.url,
                cli_args.backup_id,
                timeout=connection_timeout,
            )

        elif cli_args.action == 'ondemand':
            backup_ondemand(
                registration_code,
                cli_args.url,
                cli_args.backup_id,
                timeout=connection_timeout,
            )
        elif cli_args.action == 'list':
            response = backup_list(
                registration_code,
                cli_args.url,
                timeout=connection_timeout,
            )

        stdout.write(json.dumps(response))

    except SSBackupsException as exc:
        stdout.write(json.dumps({'detail': exc.detail, 'exit_code': exc.code}))
        exit_code = exc.code

    except Exception as exc:  # pylint: disable=broad-except
        # handle unhandled exceptions
        stdout.write(json.dumps({'detail': repr(exc), 'exit_code': ERR_CODE_UNKNOWN}))
        exit_code = ERR_CODE_UNKNOWN

    stdout.flush()

    exit(exit_code)


if __name__ == '__main__':
    main()
