#
# The client server-side-backups app is tool to manage your router's backups and thus it is a proxy
# to call remote API.
#
# Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
It provides these functions :

 * authentication against server of server-side-backups app
 * encrypt/decrypt backup file
 * call remote API to list, retrieve, delete, create router backups

It always returns exit code :
 0 - everything should be ok
 -1 .. -10 -> there is an error in current program run

When exit code is not zero than every time will appear JSON in stdout in the format:

 {"detail": "lorem ipsum, lorem ipsum...", "code": -1}

It always return additionaly data in JSON.
"""

import os
import json
import getpass
import sys

from sys import stdin
from subprocess import PIPE, Popen, check_output, CalledProcessError


# Python3 / Python2 compatibility
if sys.version_info[0] < 3:
    JsonError = ValueError
else:
    JsonError = getattr(json, "JSONDecodeError")


###
# error code meanings
#   - Remote API error
ERR_CODE_API_CALL = -1
#   - Script arguments error
ERR_CODE_ARGS = -2
#   - Arguments validation error
ERR_CODE_VALIDATE = -3
#   - Registration code error
ERR_CODE_REG_CODE = -4
#   - Encrypting error
ERR_CODE_ENCRYPT = -5
#   - Decrypting error
ERR_CODE_DECRYPT = -6
#   - Password from prompt mismatched
ERR_PASSWD_MISMATCHED = -7
#   - Max file size reached
ERR_CODE_MAX_FILE_SIZE = -8
#   - Unknown error
ERR_CODE_UNKNOWN = -10


DEFAULT_TIMEOUT = 10

# It maps method and URL path to required action
API_MAPPING = {
    'list': (
        'GET',
        '/backups/',
    ),
    'create': (
        'POST',
        '/backups/',
    ),
    'delete': (
        'DELETE',
        '/backups/{backup_id}/',
    ),
    'retrieve': (
        'GET',
        '/backups/{backup_id}/',
    ),
    'ondemand': (
        'PUT',
        '/backups/{backup_id}/on-demand/'
    ),
}


class SSBackupsException(Exception):
    """
    General client exception for this module. It has 3 attributtes.
     * detail -> verbal explanation of the exception,
     * code -> a number expressing type of exception and a exit code for this script.
    """
    def __init__(self, *args):
        super(SSBackupsException, self).__init__(*args)
        self.detail = args[0]
        self.code = ERR_CODE_API_CALL if len(args) == 1 else args[1]


def get_password(tty_recheck=False):
    """
    It gets password from stdin.

    :param tty_recheck: force reenter password when the program is used on tty
    :type tty_recheck: bool

    :return: password
    :rtype: str
    :raises SSBackupsException: when passwords mismatch
    """
    if stdin.isatty():
        # A password proper prompt
        passwd = getpass.getpass('Password for backup: ')
        if tty_recheck and (passwd != getpass.getpass('Password for backup (again): ')):
            raise SSBackupsException(
                "Passwords inserted from prompt mismatched!",
                ERR_PASSWD_MISMATCHED
            )
    else:
        # Read stdin directly when the program is triggered using pipe
        passwd = stdin.readline().rstrip()

    return passwd


def remove_encrypted_backup(backup):
    """
    It removes file of encrypted backup from FS
    :param backup: a file path where backup is or will be stored
    :return: None
    """
    os.remove(backup + '.gpg')


def encrypt_backup(backup, password):
    """
    It encryptes backup with password
    :param backup: a file path to backup
    :param password: passphrase
    :return: None
    """
    ###
    # Explanation of used gpg command arguments
    #
    # --yes                 - rewrite target file for case when it has already existed
    #
    # --batch               - Use  batch  mode.  Never ask, do not allow interactive commands.
    #                         Hide the prompt : "Reading passphrase from file descriptor 0..."
    #
    # --passphrase-fd 0     - Read the passphrase from file descriptor 0. Only the first line will
    #                         be read. The passphrase will be read from STDIN.
    #
    # --cipher-algo=AES256  - Use AES256 as cipher algorithm (default is AES128).
    #
    # -o                    - Write output to file.
    #
    # -c                    - Encrypt with a symmetric cipher using a passphrase.
    ###
    cmd = [
        "gpg", "--batch", "--yes", "--passphrase-fd", "0", "--cipher-algo=AES256",
        "-o", "{backup_path}.gpg".format(backup_path=backup),
        "-c", backup
    ]
    try:
        ###
        # stdin=PIPE -> use to pass password to gpg command
        # stderr=PIPE -> to catch error
        ###
        process = Popen(cmd, stdin=PIPE, stderr=PIPE)
        error = process.communicate(password.encode())[1]
        if process.returncode != 0:
            raise SSBackupsException(
                "Command << {cmd} >> failed having this on stderr << {error} >>.".format(
                    cmd=cmd,
                    error=error
                ), ERR_CODE_ENCRYPT)
    except CalledProcessError as exc:
        raise SSBackupsException(
            "Command << {cmd} >> ended with << return code = {return_code} >> and with output "
            "<< {output} >>.".format(
                cmd=exc.cmd,
                return_code=exc.returncode,
                output=str(exc.output)
            ), ERR_CODE_ENCRYPT)


def decrypt_backup(target, backup, password):
    """
    It decryptes backup with password
    :param target: where to store backup after downloading and decrypting
    :param backup: content of backup
    :param password: passphrase
    :type password: str
    :return: None
    """
    tmp_file = open(target + '.gpg', 'wb')
    tmp_file.write(backup)
    tmp_file.close()
    ###
    # Explanation of used gpg command arguments
    #
    # --yes                 - rewrite target file for case when it has already existed
    #
    # --batch               - Use  batch  mode.  Never ask, do not allow interactive commands.
    #                         Hide the prompt : "Reading passphrase from file descriptor 0..."
    #
    # --passphrase-fd 0     - Read the passphrase from file descriptor 0. Only the first line will
    #                         be read. The passphrase will be read from STDIN.
    #
    # -o                    - Write output to file.
    #
    # -d                    - Decrypt with a symmetric cipher using a passphrase.
    ###
    cmd = [
        "gpg", "--batch", "--yes", "--passphrase-fd", "0",
        "-o", target,
        "-d", "{target_path}.gpg".format(target_path=target)
    ]
    try:
        ###
        # stdin=PIPE -> use to pass password to gpg command
        # stderr=PIPE -> to catch error
        ###
        process = Popen(cmd, stdin=PIPE, stderr=PIPE)
        error = process.communicate(password.encode())[1]
        if process.returncode != 0:
            raise SSBackupsException(
                "Command << {cmd} >> failed having this on stderr << {error} >>.".format(
                    cmd=cmd,
                    error=error
                ), ERR_CODE_DECRYPT)
    except CalledProcessError as exc:
        raise SSBackupsException(
            "Command << {cmd} >> ended with << return code = {return_code} "
            ">> and with output << {output} >>.".format(
                cmd=exc.cmd,
                return_code=exc.returncode,
                output=str(exc.output)
            ), ERR_CODE_DECRYPT)


def get_registration_code(reg_code_path):
    """
    It returns registration code of router
    :param reg_code_path: a path to file where registration code is stored
    :return: string registration code
    """
    try:
        return open(reg_code_path, 'r').read()
    except IOError as exc:
        raise SSBackupsException(
            "Attempt to get registration code failed with message << {message} >>".format(
                message=getattr(exc, "strerror", getattr(exc, "message", repr(exc)))
            ), ERR_CODE_REG_CODE)


# pylint: disable=too-many-arguments
def call_rest_api(
    reg_code, url, action, backup_id=None, backup=None, fail=True, content_type=False,
    timeout=DEFAULT_TIMEOUT,
):
    """
    Wrapper to call remote API
    :param reg_code: a router registration code
    :param url: URL of REST API server-side-backups
    :param action: one of 'list', 'create', 'retrieve', 'delete', 'on-demand'
    :param backup_id: id of backup stored in remote database
    :param backup: a file path of backup where it is stored on local FS
    :param fail: flag says use option --fail to return exit code when HTTP 4xx and 5xx is returned
    :param content_type: flag says use option -w '%{http_code}\n%{content_type}' in curl command
    :return: response (in most cases JSON = action must be in ('list', 'create', 'ondemand'))
    :raises: SSBackupsException: when anything went wrong
    """
    # prepare data to use with curl
    # method = HTTP method, template_path = template of REST API path
    method, template_path = API_MAPPING[action]
    # path = real REST API path
    path = template_path.format(backup_id=backup_id)
    # form data = definition of payload to send it to server side via REST API
    form_data = ["-F", "payload=@%s.gpg" % backup] if backup else []
    # rest_api_url = real REST API URL
    rest_api_url = "{url}{path}".format(url=url, path=path)

    ###
    # build command curl to able to call SSBackups REST API
    # --fail triggers exit code of curl to appear when remote HTTP source
    # returns 4xx or 5xx HTTP status codes.
    ###
    cmd = ["/usr/bin/curl"]
    if fail:
        cmd.append("--fail")
    if content_type:
        cmd += ["-w", "\n%{http_code}\n%{content_type}"]
    cmd += [
        "-X", method, "-m", str(timeout),
        "-H", "Accept:application/json", "-H", "Authorization:Token %s" % reg_code
    ] + form_data + [rest_api_url]
    try:
        ###
        # stderr=open('/dev/null','w') is here to hide progress meter
        ###
        res = check_output(cmd, stderr=open('/dev/null', 'w'))
        try:
            return res.decode()
        except UnicodeDecodeError:
            return res

    except CalledProcessError as exc:
        raise SSBackupsException(
            "Command << {cmd} >> ended with << return code = {return_code} "
            ">> and with output << {output} >>.".format(
                cmd=exc.cmd,
                return_code=exc.returncode,
                output=str(exc.output)
            ), ERR_CODE_API_CALL)


def backup_list(reg_code, url, timeout=DEFAULT_TIMEOUT):
    """
    Wrapper to action list and its call_rest_api
    :param reg_code: router registration code(16 bytes length)
    :param url: url of remote REST API
    :return: list of dictionaries parsed from JSON response [{},{},...{}]
    :raises: SSBackupsException: when anything went wrong
    """
    res = call_rest_api(
        reg_code,
        url,
        'list',
        timeout=timeout,
    )
    try:
        return json.loads(res)
    except JsonError as exc:
        raise SSBackupsException(
            "JSON decoding list of backups ended with this error << {msg} >>.".format(
                msg=getattr(exc, "msg", getattr(exc, "message", repr(exc)))
            ),
            ERR_CODE_API_CALL
        )


def backup_delete(reg_code, url, backup_id, timeout=DEFAULT_TIMEOUT):
    """
    Wrapper to action delete and its call_rest_api
    :param reg_code: router registration code(16 bytes length)
    :param url: url of remote REST API
    :param backup_id: id of backup stored in remote database
    :return: None
    :raises: SSBackupsException: when anything went wrong
    """
    res = call_rest_api(
        reg_code,
        url,
        'delete',
        backup_id=backup_id,
        fail=False,
        content_type=True,
        timeout=timeout,
    )
    status_code = res.split('\n')[-2]
    if int(status_code) // 100 != 2:
        raise SSBackupsException(
            "Could not delete backup.",
            ERR_CODE_API_CALL
        )


def backup_ondemand(reg_code, url, backup_id, timeout=DEFAULT_TIMEOUT):
    """
    Wrapper to action ondemand and its call_rest_api
    :param reg_code: router registration code(16 bytes length)
    :param url: url of remote REST API
    :param backup_id: id of backup stored in remote database
    :return: None
    :raises: SSBackupsException: when anything went wrong
    """
    res = call_rest_api(
        reg_code,
        url,
        'ondemand',
        backup_id=backup_id,
        fail=False,
        content_type=True,
        timeout=timeout,
    )
    status_code = res.split('\n')[-2]
    if int(status_code) // 100 != 2:
        raise SSBackupsException(
            "Could not mark backup as on-demand.",
            ERR_CODE_API_CALL
        )


def backup_create(reg_code, url, backup, password, timeout=DEFAULT_TIMEOUT):
    """
    Wrapper to action create and its call_rest_api
    :param reg_code: router registration code(16 bytes length)
    :param url: url of remote REST API
    :param backup: a file path to backup
    :param password: passphrase for gpg to encrypt backup with AES256
    :return: string: JSON response {'id': n}, where n is positive integer
    :raises: SSBackupsException: when anything went wrong
    """

    encrypt_backup(backup, password or get_password(tty_recheck=True))

    res = call_rest_api(
        reg_code,
        url,
        'create',
        backup=backup,
        fail=False,
        content_type=True,
        timeout=timeout,
    )

    status_code, content_type = res.split('\n')[-2:]
    res = ''.join(res.split('\n')[0:-2])

    if int(status_code) // 100 == 2:
        try:
            return json.loads(res)
        except JsonError as exc:
            raise SSBackupsException(
                "JSON decoding response on create call ended with this error << {msg} >>.".format(
                    msg=getattr(exc, "msg", getattr(exc, "message", repr(exc)))
                ),
                ERR_CODE_API_CALL
            )

    remove_encrypted_backup(backup)

    if content_type == 'application/json':
        if status_code == '400':
            validation_error = json.loads(res)
            if 'payload' in validation_error:
                if [
                        err for err in validation_error['payload'] if
                        err == 'backup reaches or crosses over the limit max file size']:
                    raise SSBackupsException(
                        "Max file size reached.",
                        ERR_CODE_MAX_FILE_SIZE
                    )
    if content_type == 'text/html':
        if status_code == '413':
            raise SSBackupsException(
                "Max file size reached.",
                ERR_CODE_MAX_FILE_SIZE
            )

    raise SSBackupsException(
        "Connection error",
        ERR_CODE_API_CALL
    )


def backup_retrieve(reg_code, url, backup_id, target, password, timeout=DEFAULT_TIMEOUT):
    """
    Wrapper to action retrieve and its call_rest_api
    :param reg_code: router registration code(16 bytes length)
    :param url: url of remote REST API
    :param backup_id: id of backup stored in remote database
    :param target: a file path where backup should be stored on local FS
    :param password: passphrase for gpg to decrypt backup
    :return: None
    :raises: SSBackupsException: when anything went wrong
    """
    res = call_rest_api(
        reg_code,
        url,
        'retrieve',
        backup_id=backup_id,
        timeout=timeout,
    )

    decrypt_backup(target, res, password or get_password())

    remove_encrypted_backup(target)
